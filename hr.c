#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "http_request.h"


int main(int argc, char **argv)
{
    int opt;
    char *recv_msg = NULL;
    struct raw_http_req raw_http_req = { NULL, "80", "/", NULL, NULL };


    while ((opt = getopt(argc, argv, "h:p:i:H:d:")) != -1) {
        switch (opt) {
	    case 'h':
	        raw_http_req.host = optarg;
		break;
	    case 'p':
	        raw_http_req.port = optarg;
		break;
	    case 'i':
	        raw_http_req.uri = optarg;
		break;
	    case 'H':
	        raw_http_req.extra_header = optarg;
		break;
	    case 'd':
	        raw_http_req.data = optarg;
		break;
	}
    }

    if (raw_http_req.host)
        recv_msg = http_request(&raw_http_req);

    if (recv_msg) puts(recv_msg);
    free(recv_msg);


    return 0;
}

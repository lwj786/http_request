#! /usr/bin/env -S guile3 -s
!#

(use-modules (system foreign))


(define c_free (pointer->procedure void
				   (dynamic-func "free" (dynamic-link))
				   '(*)))

(define (http_request host port uri extra_header data)
  (let* ((libhr (dynamic-link (if (file-exists? "./libhr.so.0") "./libhr.so.0" "libhr.so.0")))
	 (c_http_request (pointer->procedure '*
					     (dynamic-func "http_request" libhr)
					     '(*)))

	 (string?->pointer (lambda (s) (if (string? s) (string->pointer s) %null-pointer)))
	 (c_raw_http_req (make-c-struct '(* * * * *)
					`(,(string?->pointer host)
					  ,(string?->pointer port)
					  ,(string?->pointer uri)
					  ,(string?->pointer extra_header)
					  ,(string?->pointer data))))

	 (c_recv_msg (c_http_request c_raw_http_req)))

    (if (null-pointer? c_recv_msg) '("" . %null-pointer) `(,(pointer->string c_recv_msg) . ,c_recv_msg))))


(define recv_msg (http_request "example.com" "80" "/" "" ""))
(display (car recv_msg))
(c_free (cdr recv_msg))

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>

#include "http_request.h"


#define strlen_s(s) \
    ((s) ? strlen((s)) : 0)

inline size_t ilen(int n)
{
    size_t i = 0;

    for (; n /= 10; ++i) ;

    return i;
}


char *make_http_request(struct raw_http_req *raw_http_req)
{
    char *http_req = NULL, *empty_str = "",
    *http_get_req_template =
        "GET %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "Accept: */*\r\n"
        "%s\r\n"
        "\r\n",
    *http_post_req_template =
        "POST %s HTTP/1.1\r\n"
        "Host: %s\r\n"
        "Accept: */*\r\n"
        "%s\r\n"
        "Content-Length: %d\r\n"
        "Content-Type: application/x-www-form-urlencoded\r\n"
        "\r\n"
        "%s";
    size_t data_len = 0, req_len = 0;


    if (raw_http_req->data && strlen(raw_http_req->data)) {
        data_len = strlen(raw_http_req->data);

        req_len = strlen(http_post_req_template) - 10
            + strlen(raw_http_req->host)
            + strlen(raw_http_req->uri)
	    + strlen_s(raw_http_req->extra_header)
	    + ilen(data_len)
	    + strlen(raw_http_req->data);

        http_req = (char *)malloc((req_len + 1) * sizeof(char));

        if (http_req
	        && sprintf(http_req,
                    http_post_req_template,
		    raw_http_req->uri,
		    raw_http_req->host,
		    raw_http_req->extra_header ?: empty_str,
		    data_len,
		    raw_http_req->data) < 0) {
            free(http_req);
            http_req = NULL;
        }
    } else {
        req_len = strlen(http_get_req_template) - 6
            + strlen(raw_http_req->host)
            + strlen(raw_http_req->uri)
	    + strlen_s(raw_http_req->extra_header);

        http_req = (char *)malloc((req_len + 1) * sizeof(char));
    
        if (http_req
	        && sprintf(http_req,
                    http_get_req_template,
		    raw_http_req->uri,
		    raw_http_req->host,
		    raw_http_req->extra_header ?: empty_str) < 0) {
            free(http_req);
            http_req = NULL;
        }
    }

    MY_DEBUG_OUTPUT(http_req);


    return http_req;
}

int send_http_request(struct raw_http_req *raw_http_req)
{
    struct addrinfo hints, *res, *r;
    char *http_req = NULL;
    int sockfd = -1;
    ssize_t s = 0, nw = 0;


    memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = 0;

    if (!getaddrinfo(raw_http_req->host, raw_http_req->port, &hints, &res)) {
        for (r = res; r; r = r->ai_next) {
	    sockfd = socket(r->ai_family, r->ai_socktype, r->ai_protocol);

	    if (sockfd != -1) {
	        if (connect(sockfd, r->ai_addr, r->ai_addrlen) != -1)
		    break;

		close(sockfd);
		sockfd = -1;
	    }
	}

	freeaddrinfo(res);
    }

    if (sockfd != -1) {
        http_req = make_http_request(raw_http_req);
	if (http_req) {
	    do {
	        s = write(sockfd, http_req + nw, strlen(http_req + nw));
		nw += s;
		if (s == -1) break;
	    } while (strlen(http_req + nw));

	    free(http_req);

	    if (s == -1) {
	        close(sockfd);
		sockfd = -1;
	    }
	}
    }


    return sockfd;
}

char *http_request(struct raw_http_req *raw_http_req)
{
    char *recv_msg = NULL, *cp = NULL;
    ssize_t s = 0, nr = 0, recv_msg_size = 0;
    int sockfd = -1, c = 1;


    sockfd = send_http_request(raw_http_req);

    if (sockfd != -1) {
        recv_msg = (char *)malloc(c * MSG_BUF_SIZE * sizeof(char));
	memset(recv_msg, 0, MSG_BUF_SIZE);

        do {
            s = read(sockfd, recv_msg + nr, c * MSG_BUF_SIZE - nr);

	    if ( s == -1) {
	        free(recv_msg);
		recv_msg = NULL;
		break;
	    }

	    nr += s;

	    if (!recv_msg_size && (cp = strstr(recv_msg, "\r\n\r\n"))) {
	        recv_msg_size = cp - recv_msg + 4;

		if (cp = strstr(recv_msg, "Content-Length: ")) {
		    cp += sizeof("Content-Length: ") - 1;
		    *(strchr(cp, '\r')) = '\0';

		    recv_msg_size += atoi(cp);

		    *(strchr(cp, '\0')) = '\r';
		} else {
		    recv_msg_size = -1;
		}
	    }

	    if (nr == c * MSG_BUF_SIZE) {
	        recv_msg = (char *)realloc(recv_msg, ++c * MSG_BUF_SIZE * sizeof(char));
		memset(recv_msg + (c - 1) * MSG_BUF_SIZE, 0, MSG_BUF_SIZE);
	    }

	    if (recv_msg_size && nr == recv_msg_size) break;
	    if (recv_msg_size == -1 && strstr(recv_msg, "0\r\n\r\n")) break;
        } while(1);

        close(sockfd);
    }

    MY_DEBUG_OUTPUT(recv_msg);


    return recv_msg;
}

#ifndef __MY_HTTP_REQUEST__

#define __MY_HTTP_REQUEST__

#define MSG_BUF_SIZE 1024

struct raw_http_req {
    char *host;
    char *port;
    char *uri;
    char *extra_header;
    char *data;
};

char *http_request(struct raw_http_req *raw_http_req);

#define MY_DEBUG_OUTPUT(s) \
    if (getenv("HTTP_REQUEST_DEBUG") && (s)) fputs((s), stderr);

#endif

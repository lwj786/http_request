CFLAGS = -g -O2

libhr.so.0: http_request.c http_request.h
	$(CC) -o $@ \
		$(CFLAGS) \
		-fPIC -shared -Wl,-soname=libhr.so.0 \
		$<

hr: hr.c libhr.so.0
	$(CC) -o $@ \
		$(CFLAGS) \
		-Wl,-rpath='$$ORIGIN' \
		$^

clean:
	rm -rf hr libhr.so.0

.PHONY: clean
